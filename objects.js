var studentsAndPoints = ['Алексей Петров', 0, 'Ирина Овчинникова', 60, 'Глеб Стукалов', 30, 'Антон Павлович', 30, 'Виктория Заровская', 30, 'Алексей Левенец', 70, 'Тимур Вамуш', 30, 'Евгений Прочан', 60, 'Александр Малов', 0]


// заполнение массива students

function newStudentsArray(student, point) {
  var student = {
    student: student,
    point: point,
    show: function () {
      console.log("Студент %s набрал %s баллов", this.student, this.point)
    }
  }
  return student
}

var students = []

for (var i = 0; i < studentsAndPoints.length; i+=2) {
  students.push(newStudentsArray(studentsAndPoints[i], studentsAndPoints[i+1]))
}


//добавить "Николай Фролов" и "Олег Боровой"

students.push(newStudentsArray('Николай Фролов', 0))
students.push(newStudentsArray('Олег Боровой', 0))


//увеличить баллы Ирине Овчинниковой, Александру Малову и Николаю Фролову

students.forEach(function (value) {
  if (value.student === 'Ирина Овчинникова' || value.student === 'Александр Малов') {
    value.point += 30
  } else if (value.student === 'Николай Фролов'){
    value.point += 10
  }
})



// Вывести список студентов набравших 30 и более баллов без использования циклов

console.log('Список студентов:')
students.forEach (function (value) {
  if (value.point >= 30) {
    value.show()
  }
})


//Учитывая что каждая сделанная работа оценивается в 10 баллов, добавить всем студентам поле worksAmount равное кол-ву сделанных работ.

for (var i = 0; i < students.length; i++) {
  students[i].worksAmount = students[i].point / 10
}
